# ssh-keypair

## Summary

This module creates an AWS key pair to be used for authentication to AWS resources. Prior to using this module, a key pair must be created by using ssh-keygen or [PuTTYgen](https://www.chiark.greenend.org.uk/~sgtatham/putty/).

## Resources

This module creates the following resources:

* [aws_key_pair](https://www.terraform.io/docs/providers/aws/r/key_pair.html)

## Cost

Using this module on its own doesn't cost anything.

## Inputs

| Name        | Description                                                                                                                                         | Type          | Default | Required |
|:------------|:----------------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:--------|:---------|
| application | Name of the application the resource(s) will be a part of.                                                                                          | `string`      | -       | yes      |
| environment | Name of the environment the resource(s) will be a part of. Used to lookup the VPC by name.                                                          | `string`      | -       | yes      |
| role        | Name of the role the resource(s) will be performing.                                                                                                | `string`      | -       | yes      |
| lifespan    | The intended lifespan of the resource(s). Acceptable values: `temporary` - Can be destroyed after use or testing; `permanent` - Cannot be destroyed | `string`      | -       | yes      |
| tags        | Additional tags to attach to all resources created by this module.                                                                                  | `map(string)` | `{}`    | no       |
| public_key  | The public key for this key pair.                                                                                                                   | `string`      | -       | yes      |

## Outputs

| Name     | Description               |
|:---------|:--------------------------|
| key_name | The name of the key pair. |

## Examples

### Simple Usage

This example assumes the public key of the key pair is stored with the module in gitlab in a file named `example_key.pub`.

```terraform
module "ssh_keypair" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/ssh-keypair.git?ref=v1"

  application = "example_app"
  environment = "example_env"
  role        = "example_role"
  lifespan    = "temporary"

  public_key  = file("${path.module}/example_key.pub")
}
```

## Version History

* v1 - Initial Release
