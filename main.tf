module "tags_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v1"

  application = var.application
  environment = var.environment
  lifespan    = var.lifespan
  tags        = var.tags
}

resource "aws_key_pair" "key_pair" {
  key_name = "${var.application}-${var.environment}-${var.role}"

  public_key = var.public_key

  tags = module.tags_base.tags
}
